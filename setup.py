import os
import setuptools

with open("README.md") as file_handler:
    long_description = file_handler.read()

if os.environ.get('CI_COMMIT_TAG'):
    version = os.environ['CI_COMMIT_TAG']
else:
    version = os.environ['CI_JOB_ID']

setuptools.setup(
    name = "example_tests_module",
    version = version,
    author = "Damian 'Draqun' Giebas",
    author_email = "damian.giebas@gmail.com",
    description = "Test description",
    license = "GNU/GPLv3",
    long_description = long_description,
    long_description_content_type = "text/markdown",
    url = "",
    packages = setuptools.find_packages(),
    platforms = ["Windows", "Linux", "Solaris", "Mac OS-X", "Unix"],
    classifiers = [
        "Programming Language :: Python :: 3",
        "Topic :: Education :: Testing",
    ]
)
